package com.jpop.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jpop.userservice.persistence.model.UserEntity;

@Repository
public interface UserRepository  extends JpaRepository<UserEntity, Long>{

}
