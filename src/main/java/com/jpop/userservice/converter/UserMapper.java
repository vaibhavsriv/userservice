package com.jpop.userservice.converter;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.jpop.userservice.dto.UserDTO;
import com.jpop.userservice.persistence.model.UserEntity;

@Mapper
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper( UserMapper.class ); 
	    
    UserDTO userEntityToDto(UserEntity user);
    
    UserEntity userDTOToEntity(UserDTO user);
}
