package com.jpop.userservice.service;

import java.util.List;

import com.jpop.userservice.dto.UserDTO;


public interface UserServiceDelegate {
	
	public UserDTO save(UserDTO UserDTO);

	public UserDTO update(UserDTO UserDTO, Long id);

	public void deleteUser(Long id);

	public UserDTO getUserDetails(Long id);

	public List<UserDTO> getAllUsers();

}
