package com.jpop.userservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.jpop.userservice.converter.UserMapper;
import com.jpop.userservice.dto.UserDTO;
import com.jpop.userservice.exception.UserNotFoundException;
import com.jpop.userservice.persistence.model.UserEntity;
import com.jpop.userservice.repository.UserRepository;

@Service
public class UserServiceDelegateImpl implements UserServiceDelegate {

	@Autowired
	UserRepository userRepository;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public UserDTO save(UserDTO userDTO) {
		UserEntity user=UserMapper.INSTANCE.userDTOToEntity(userDTO);
		user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
		UserEntity savedUser=userRepository.save(user);
		return UserMapper.INSTANCE.userEntityToDto(savedUser);
	}

	@Override
	public UserDTO update(UserDTO userDTO, Long id) {
		UserEntity newUser = UserMapper.INSTANCE.userDTOToEntity(userDTO);
		return userRepository.findById(id).map(x -> {
			x.setFirstName(newUser.getFirstName());
			x.setLastName(newUser.getLastName());
			x.setPassword(bCryptPasswordEncoder.encode(newUser.getPassword()));
			x.setEmail(newUser.getEmail());
			x.setRoles(newUser.getRoles());
			UserEntity entity = userRepository.save(x);
			return UserMapper.INSTANCE.userEntityToDto(entity);
		}).orElseGet(() -> {
			newUser.setId(id);
			UserEntity entity = userRepository.save(newUser);
			return UserMapper.INSTANCE.userEntityToDto(entity);

		});
	}

	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);

	}

	@Override
	public UserDTO getUserDetails(Long id) {
		return userRepository.findById(id).map(entity ->  UserMapper.INSTANCE.userEntityToDto(entity))
				.orElseThrow(() -> new UserNotFoundException(id));
	}

	@Override
	public List<UserDTO> getAllUsers() {
		List<UserDTO> userList = new ArrayList<>();
		userRepository.findAll().forEach(user -> userList.add(UserMapper.INSTANCE.userEntityToDto(user)));
		return userList;
	}

}
