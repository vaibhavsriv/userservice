package com.jpop.userservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.userservice.dto.UserDTO;
import com.jpop.userservice.service.UserServiceDelegate;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/v1")
public class UserServiceController {

	@Autowired
	private UserServiceDelegate userServiceDelegate;

	@GetMapping("/users")
	public List<UserDTO> getAllUsers() {
		return userServiceDelegate.getAllUsers();
	}

	@PostMapping("/users")
	@ApiOperation(value = "Add a new user")
	public UserDTO saveBook(@RequestBody UserDTO newUser) {
		return userServiceDelegate.save(newUser);
	}

	@GetMapping("/users/{id}")
	public UserDTO getUserDetails(@PathVariable Long id) {
		return userServiceDelegate.getUserDetails(id);

	}

	@PutMapping("/users/{id}")
	public UserDTO updateUser(@RequestBody UserDTO newUser, @PathVariable Long id) {

		return userServiceDelegate.update(newUser, id);
	}

	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable Long id) {
		userServiceDelegate.deleteUser(id);
	}

	public UserServiceDelegate getUserServiceDelegate() {
		return userServiceDelegate;
	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}

}
