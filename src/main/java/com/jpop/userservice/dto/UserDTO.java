package com.jpop.userservice.dto;

import java.util.List;

import lombok.Data;

@Data
public class UserDTO {
	private Long id;

	private String firstName;

	private String lastName;

	private String email;

	private String password;

	private boolean enabled;

	private List<RoleDTO> roles;

}
