package com.jpop.userservice;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jpop.userservice.enums.RoleEnum;
import com.jpop.userservice.persistence.model.RoleEntity;
import com.jpop.userservice.persistence.model.UserEntity;
import com.jpop.userservice.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class UserServiceApplicationTests {

	private static final ObjectMapper om = new ObjectMapper();
	@Autowired
    private WebApplicationContext context;
	
	
	private MockMvc mockMvc;

	@MockBean
	UserRepository mockRepository;
	@Before
    public void setup() {
		mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

	
	@Before
	public void init() {

		UserEntity user = generateTestData("Test User", "Test");

		when(mockRepository.findById(1L)).thenReturn(Optional.of(user));

	}
	@WithMockUser("user")
	@Test
	public void find_bookId_OK() throws Exception {

		mockMvc.perform(get("/users/2001"))
				 .andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.firstName", is("Test User")))
				.andExpect(jsonPath("$.password", is("Test")));

		verify(mockRepository, times(1)).findById(1L);

	}
	
		
	private UserEntity generateTestData(String name, String password) {
		RoleEntity role = new RoleEntity();
		role.setId(2L);
		role.setName(RoleEnum.ADMIN.name());

		
		Set<RoleEntity> ruleSet = new HashSet<RoleEntity>();
		ruleSet.add(role);
		
		UserEntity user = new UserEntity();
		user.setId(1L);
		user.setFirstName(name);
		user.setLastName(name);
		user.setEmail("test@test.com");
		user.setEnabled(true);
		user.setPassword(password);
		
		user.setRoles(ruleSet);
		return user;
	}

}
